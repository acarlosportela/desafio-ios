//
//  Repository.swift
//  desafio-ios
//
//  Created by Antonio Rodrigues on 16/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import Foundation
import UIKit

struct Repository{
    
    var name: String
    var description: String
    var author: String
    var avatarUrl: String
    var stars: String
    var forks: String
    
}
