//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Antonio Rodrigues on 16/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import Foundation
import UIKit

struct PullRequest{
    
    var title: String
    var author: String
    var avatarUrl: String
    var date: String
    var body: String
    var url : String
    
}
