//
//  ReposCell.swift
//  desafio-ios
//
//  Created by Antonio Rodrigues on 16/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import UIKit

class ReposCell: UITableViewCell {
    
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var repoDesc: UILabel!
    @IBOutlet weak var repoForks: UILabel!
    @IBOutlet weak var repoStars: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var owner: UILabel!
    
}
