//
//  PullRequestsCell.swift
//  desafio-ios
//
//  Created by Antonio Rodrigues on 16/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import UIKit

class PullRequestsCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var createdAt: UILabel!

}
