//
//  ReposTableViewController.swift
//  desafio-ios
//
//  Created by Antonio Rodrigues on 16/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON

class ReposTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var reposTableView: UITableView!
    let gitApiReposUrl = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
    let threshold = 5
    var page = 1
    var canFetchMore = true
    var repositories = [Repository]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reposTableView.rowHeight = UITableViewAutomaticDimension
        self.reposTableView.estimatedRowHeight = 140
    }
    
    override func viewWillAppear(_ animated: Bool) {
        page = 1
        self.viewLoading.isHidden = false
        getRepos(page: page)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReposCell", for: indexPath) as! ReposCell
        let repo = repositories[indexPath.row]
        
        cell.repoName.text = repo.name
        cell.repoDesc.text = repo.description
        cell.repoStars.text = repo.stars
        cell.repoForks.text = repo.forks
        cell.owner.text = repo.author
        cell.avatar.kf.setImage(with: URL(string: repo.avatarUrl))
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    //Usado para checar se está no fim do scroll da table view e carregar mais dados
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (self.repositories.count - indexPath.row) == self.threshold && self.canFetchMore {
            self.page += 1
            self.viewLoading.isHidden = false
            self.getRepos(page: self.page)
        }
    }

    // Ao tocar em um item da tableView, levar apara o PullRequestsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPullRequests"?:
            if let row = self.reposTableView.indexPathForSelectedRow?.row{
                let pullRequestsViewController = segue.destination as! PullRequestsViewController
                pullRequestsViewController.repository = repositories[row]
            }
        default:
            preconditionFailure("Unexpeted Segue")
        }
    }
}

extension ReposTableViewController{
    
    func getRepos(page:Int){
        
        let url = String(format: "%@%d", self.gitApiReposUrl, page)
        
        //Usando autenticação básica devido ao rate limit da api do github
        Alamofire.request(url)
            .validate(contentType: ["application/json"])
            .authenticate(user: "acportela2", password: "desafioios1")
            .responseJSON { response in
                switch response.result {
                case .success:
                    
                    let json = JSON(response.data!)
 
                    if let _ = json["message"].string{
                        self.canFetchMore = false
                    }
                    
                    let repos: [Repository] = json["items"].arrayValue.map({
                        return Repository(name: $0["name"].stringValue, description: $0["description"].stringValue, author: ($0["owner"]["login"]).stringValue, avatarUrl: ($0["owner"]["avatar_url"]).stringValue, stars: $0["stargazers_count"].stringValue, forks: $0["forks_count"].stringValue)
                    })
                    
                    self.repositories += repos
                    self.reposTableView.reloadData()
                    self.viewLoading.isHidden = true
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
}
