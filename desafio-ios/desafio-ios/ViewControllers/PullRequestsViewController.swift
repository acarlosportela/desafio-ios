//
//  PullRequestsViewController.swift
//  desafio-ios
//
//  Created by Mobilicidade ltda on 18/01/18.
//  Copyright © 2018 Antonio Rodrigues. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON

class PullRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var pullRequestsTableView: UITableView!
    let gitHubApiPullRequestsUrl = "https://api.github.com/repos/%@/%@/pulls"
    var repository: Repository?
    var pullRequests = [PullRequest]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.pullRequestsTableView.rowHeight = UITableViewAutomaticDimension
        self.pullRequestsTableView.estimatedRowHeight = 140
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let name = repository?.name, let owner = repository?.author{
            getPullRequests(repoName: name, owner: owner)
        }
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestsCell", for: indexPath) as! PullRequestsCell
        let pullRequest = pullRequests[indexPath.row]
        
        cell.title.text = pullRequest.title
        cell.body.text = pullRequest.body
        cell.author.text = pullRequest.author
        cell.createdAt.text = pullRequest.date
        cell.avatar.kf.setImage(with: URL(string: pullRequest.avatarUrl))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = self.pullRequests[indexPath.row]
        if let url = URL(string: pullRequest.url){
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
}

extension PullRequestsViewController{
    
    func getPullRequests(repoName: String, owner: String){
        
        let url = String(format: self.gitHubApiPullRequestsUrl, owner,repoName)
      
        //Usando autenticação básica devido ao rate limit da api do github
        Alamofire.request(url)
            .validate(contentType: ["application/json"])
            .authenticate(user: "acportela2", password: "desafioios1")
            .responseJSON { response in
                switch response.result {
                case .success:
                    let pullReqs = JSON(response.data!).arrayValue.map {
                        PullRequest(title: $0["title"].stringValue, author: ($0["user"]["login"]).stringValue, avatarUrl: ($0["user"]["avatar_url"]).stringValue, date: $0["created_at"].stringValue, body: $0["body"].stringValue, url: $0["html_url"].stringValue)
                    }
                    self.pullRequests = pullReqs
                    self.pullRequestsTableView.reloadData()
                case .failure(let error):
                    print(error)
                }
        }
    }
    
}
